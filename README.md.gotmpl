# Redis Helm chart

{{ template "chart.deprecationWarning" . }}

{{ template "chart.badgesSection" . }}

This repository contains a [Helm](https://helm.sh/) chart to deploy [Redis](https://redis.com) on Kubernetes.
Redis is an open-source in-memory storage, used as a distributed, in-memory key–value database, cache and message broker.

The chart is used as a subchart to other charts.

We use the Bitnami docker image of [Redis](https://hub.docker.com/r/bitnami/redis).

{{ template "chart.maintainersSection" . }}

{{ template "chart.sourcesSection" . }}

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}

## License

This repository is based on the work from the [Bitnami Redis chart](https://github.com/bitnami/charts/tree/master/bitnami/redis) which is licensed under the [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
The modifications and contents of this repository are licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

{{ template "helm-docs.versionFooter" . }}
