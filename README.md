# Redis Helm chart

![Version: 1.1.1](https://img.shields.io/badge/Version-1.1.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 6.2.13](https://img.shields.io/badge/AppVersion-6.2.13-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy [Redis](https://redis.com) on Kubernetes.
Redis is an open-source in-memory storage, used as a distributed, in-memory key–value database, cache and message broker.

The chart is used as a subchart to other charts.

We use the Bitnami docker image of [Redis](https://hub.docker.com/r/bitnami/redis).

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Set Redis container's Security Context allowPrivilegeEscalation |
| containerSecurityContext.runAsUser | int | `2000` | Set Redis container's Security Context runAsUser. UID 2000 is reserved for a non-privileged user dedicated for running container processes at the node. |
| existingSecret.name | string | `"redis-credentials"` | Name of the existing secret containing the Redis password |
| existingSecret.passwordKey | string | `"password"` | Key containing the Redis password |
| fullnameOverride | string | `""` | String to fully override redis.fullname |
| image.pullPolicy | string | `"IfNotPresent"` | Redis image pull policy |
| image.repository | string | `"bitnami/redis"` | Redis image repository |
| image.tag | string | Chart.appVersion | Redis image tag |
| livenessProbe.enabled | bool | `true` | Enable livenessProbe on Redis Primary containers |
| livenessProbe.failureThreshold | int | `5` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `5` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `6` | Timeout seconds for livenessProbe This should be at least one second longer than the probe command timeout, which is currently set to 5, in order to prevent a generation of zombie processes. |
| nameOverride | string | `""` | String to partially override redis.fullname |
| podManagementPolicy | string | `"OrderedReady"` | Pod management policy for the Redis statefulset |
| podSecurityContext.fsGroup | int | `2000` | Set Redis pod's Security Context fsGroup.  UID 2000 is reserved for a non-privileged user dedicated for running container processes at the node. |
| readinessProbe.enabled | bool | `true` | Enable readinessProbe on Redis containers |
| readinessProbe.failureThreshold | int | `5` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `5` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `2` | Timeout seconds for readinessProbe This should be at least one second longer than the probe command timeout, which is currently set to 1, in order to prevent a generation of zombie processes. |
| replicaCount | int | `1` | Number of Redis replicas to deploy |
| resources.limits.memory | string | `"128Mi"` | The memory limits for the Redis containers |
| resources.requests.cpu | string | `"50m"` | The requested cpu for the Redis containers |
| resources.requests.memory | string | `"64Mi"` | The requested memory for the Redis containers |
| service.port | int | `6379` | Redis service port |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for Redis pod. If set to false, the default serviceAccount is used. |
| serviceAccount.name | string | redis.fullname | The name of the service account to use. |
| startupProbe.enabled | bool | `true` | Enable startupProbe on Redis containers |
| startupProbe.failureThreshold | int | `5` | Failure threshold for startupProbe |
| startupProbe.initialDelaySeconds | int | `20` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `5` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| terminationGracePeriodSeconds | string | `""` | Seconds Redis pod needs to terminate gracefully |
| updateStrategy.rollingUpdate | object | `{}` | Redis statefulset rolling update configuration parameters |
| updateStrategy.type | string | `"RollingUpdate"` | Redis statefulset update strategy type |

## License

This repository is based on the work from the [Bitnami Redis chart](https://github.com/bitnami/charts/tree/master/bitnami/redis) which is licensed under the [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
The modifications and contents of this repository are licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
